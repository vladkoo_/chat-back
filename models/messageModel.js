const mongoose = require('mongoose');

const {Schema} = mongoose;

const messageSchema = new Schema({
    fromUserId: {type: Schema.Types.ObjectId, require: true},
    fromRoomId: {type: Schema.Types.ObjectId, require: true},
    text: {type: String, require: true},
    postDate: {type: Date, require: true}
}, {versionKey: false});

const messageModel = mongoose.model('message', messageSchema);

module.exports = messageModel;
