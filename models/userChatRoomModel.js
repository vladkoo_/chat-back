const mongoose = require('mongoose');

const {Schema} = mongoose;

const userChatRoomSchema = new Schema({
    userId: {type: Schema.Types.ObjectId, require: true},
    chatRoomId: {type: Schema.Types.ObjectId, require: true},
    roomCreator: {type: Schema.Types.ObjectId, require: true},
    joinedAt: {type: Date, require: true}
}, {versionKey: false});

const userChatRoomModel = mongoose.model('userChatRoom', userChatRoomSchema);

module.exports = userChatRoomModel;
