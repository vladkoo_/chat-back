const mongoose = require('mongoose');

const {Schema} = mongoose;

const userSchema = new Schema({
    login: {type: String, require: true, unique: true},
    email: {type: String, require: true, unique: true},
    password: {type: String, require: true},
    phone: {type: String, require: false},
    firstName: {type: String, require: false},
    lastName: {type: String, require: false},
    avatar: {type: String, require: false}
}, {versionKey: false});

const userModel = mongoose.model('user', userSchema);

module.exports = userModel;
