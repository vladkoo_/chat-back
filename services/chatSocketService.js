const MessageModel = require('../models/messageModel');
const UserChatRoomModel = require('../models/UserChatRoomModel');
const mongoose = require('mongoose');

module.exports = function (io) {
    io.on('connection', (socket) => {
        console.log('new connection made');

        socket.on('disconnect', function (data) {
            console.log('disconnect');
        });

        socket.on('join guest room', (data) => {
            socket.join('guestroom');
            console.log('JOINED GUEST ROOM');
        });

        socket.on('leave guest room', (data) => {
            console.log('LEFT GUEST ROOM');
            socket.leave('guestroom');
        });

        socket.on('join room', (data) => {
            socket.join(data.roomId);
            console.log('JOINED ROOM');
        });

        socket.on('leave room', (room) => {
            console.log('LEFT ROOM');
            socket.leave(room.roomId);
        });

        socket.on('add new room', (newRoom) => {
            console.log(newRoom);
            console.log('ADDED NEW ROOM');
            io.in('guestroom').emit('added new room', newRoom);
        });

        socket.on('delete room', (roomId) => {
            console.log(roomId);
            console.log('DELETED ROOM');
            io.in('guestroom').emit('deleted room', roomId);
        });


        socket.on('get message list', (data) => {
            console.log('GOT MESSAGES');
            UserChatRoomModel.find({userId: data.userId, chatRoomId: data.roomId})
                .then((userRoom) => {
                    if (userRoom === undefined || userRoom.length === 0) {
                        console.log('nothing was found');
                        UserChatRoomModel.create({userId: data.userId, chatRoomId: data.roomId, joinedAt: new Date()})
                            .then((newUserRoom) => {
                                socket.emit('got message list', []);
                            });
                    } else {
                        // console.log('something is found');
                        MessageModel.aggregate([
                            {
                                $match: {
                                    "fromRoomId": {$eq: mongoose.Types.ObjectId(data.roomId)},
                                    "postDate": {$gte: userRoom[0].joinedAt}
                                }
                            },
                            {
                                $lookup: {
                                    'from': 'users',
                                    'localField': 'fromUserId',
                                    'foreignField': '_id',
                                    'as': 'Username'
                                }
                            },
                            {
                                $unwind: {
                                    'path': '$Username',
                                    'preserveNullAndEmptyArrays': true
                                }
                            },
                            {
                                $group: {
                                    "_id": '$_id',
                                    "fromUserId": {
                                        $first: '$fromUserId'
                                    },
                                    "fromUserName": {
                                        $first: '$Username.login'
                                    },
                                    "text": {
                                        $first: "$text"
                                    },
                                    "postDate": {
                                        $first: "$postDate"
                                    }
                                }
                            },
                            {
                                $sort: {"postDate": 1}
                            }
                        ]).then((messageList) => {
                            console.log(messageList);
                            socket.emit('got message list', messageList);
                        })

                    }
                }).catch((error) => {
                console.log('something is wrong with socket service');
                console.log(error);
            });


        });

        socket.on('new message', (message) => {
            console.log('NEW MESSAGE RECEIVED');
            MessageModel.create({
                ...message,
                postDate: new Date()
            }).then((newMessage) => {
                io.in(newMessage.fromRoomId).emit('new message received', {
                    ...newMessage._doc,
                    fromUserName: message.fromUserName
                });
            });
        });


    });
};
