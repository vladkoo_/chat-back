const ChatRoomModel = require('../models/chatRoomModel');
const UserChatRoomModel = require('../models/UserChatRoomModel');

class ChatRoomService {
    createRoom(req, res, next) {
        ChatRoomModel.create({name: req.body.name})
            .then((newRoom) => {
                const today = new Date();
                const date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
                const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                const dateTime = date + " " + time;
                console.log(dateTime);
                UserChatRoomModel.create({
                    userId: req.body.roomCreator,
                    chatRoomId: newRoom._id,
                    roomCreator: req.body.roomCreator,
                    joinedAt: today
                })
                    .then((newUserChatRoom) => {
                        res.send({_id: newRoom._id, name: newRoom.name, roomCreator: newUserChatRoom.userId});
                    })

            })
            .catch((error) => {
                res.status(400).send(error);
            })
    }

    async getRoomsList(req, res, next) {
        const data = await UserChatRoomModel.aggregate([
            {
                $lookup: {
                    'from': 'chatrooms',
                    'localField': 'chatRoomId',
                    'foreignField': '_id',
                    'as': 'Rooms'
                }
            },
            {
                $unwind: {
                    'path': '$Rooms',
                    'preserveNullAndEmptyArrays': true
                }
            },
            {
                $group: {
                    "_id": '$Rooms._id',
                    "name": {
                        $first: '$Rooms.name'
                    },
                    "roomCreator": {
                        $first: "$roomCreator"
                    },
                }
            }
        ]);
        res.send(data.reverse());
    }

    deleteRoom(req, res, next) {
        ChatRoomModel.findOneAndDelete({_id: req.params.id})
            .then((info) => {
                UserChatRoomModel.deleteMany({chatRoomId: req.params.id})
                    .then((info) => {
                        res.send(info);
                    })
            })
            .catch((error) => {
                res.status(500).send(error);
            })
    }

}

let chatRoomService = new ChatRoomService();
module.exports = chatRoomService;
